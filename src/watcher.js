const fs = require("fs");
const path = require("path");
const readline = require("readline");

fs.watch(path.join(__dirname, `../dist`), (eventType, fileName) => {
  if (eventType !== "change") {
    return;
  }

  const fileWhatWasChanged = path.join(__dirname, "../dist", fileName);
  const content = fs.readFileSync(fileWhatWasChanged);
  const parsedData = JSON.parse(content.toString());
  const csvRow = `\n${parsedData.date},${parsedData.value},${parsedData.name}`;

  switch (parsedData.type) {
    case "Gas":
      fs.appendFile(path.join(__dirname, "../audit/gas.csv"), csvRow, () =>
        console.log("gas updated")
      );
      break;
    case "Water":
      fs.appendFile(path.join(__dirname, "../audit/water.csv"), csvRow, () =>
        console.log("water updated")
      );
      break;
  }

  fs.unlinkSync(fileWhatWasChanged);
});
