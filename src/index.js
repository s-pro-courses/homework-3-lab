const fs = require("fs");
const path = require("path");
const readline = require("readline");

const rl = readline.createInterface({
  output: process.stdout,
  input: process.stdin,
});

function askSynk(q, validateAnswer) {
  return new Promise((resolve, reject) => {
    rl.question(q, (data) => {
      if (typeof validateAnswer === "function" && !validateAnswer(data)) {
        reject(new Error(`answer error at the question : ${q}`));
      }
      resolve(data);
    });
  });
}
async function asker() {
  try {
    let name = await askSynk(
      "What is you name?(min 3 , max 20) ",
      (answer) => answer.length > 2 && answer.length < 20
    );
    let value = await askSynk(
      "What is value?(6 symbols) ",
      (answer) => answer.length === 6
    );
    let type = await askSynk(
      "Select and type Water or Gas? ",
      (answer) => answer === "Water" || answer === "Gas"
    );
    let date = new Date();

    fs.writeFile(
      path.join(__dirname, `../dist/${name}-${Date.now()}.json`),
      JSON.stringify({
        name,
        value,
        type,
        date: `${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`,
      }),
      (error) => {
        if (error) {
          throw error;
        }
      }
    );
  } catch (error) {
    console.log(error.message);
  } finally {
    rl.close();
  }
}

asker();
